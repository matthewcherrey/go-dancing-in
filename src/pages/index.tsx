import * as React from "react"
import type { HeadFC, PageProps } from "gatsby"

import InlineTitle from "../Components/InlineTitle"
import Select, { GroupBase } from 'react-select'

import "../css/index.css"

interface optionPair{
  value: string,
  label: string
}

const bigStyle = {
  control: (base: any) => ({
    ...base,
    height: 100,
    minHeight: 30
  })
};

const dances: Array<optionPair> = [
  { value: "Swing", label: "Swing"},
  { value: "Salsa", label: "Salsa"}
]

const locations: Array<optionPair> = [
  { value: "New York City", label: "New York City"},
  { value: "London", label: "London"},
  { value: "Stockholm", label: "Stockholm"}
]

const IndexPage: React.FC<PageProps> = () => {
  return (
    <>
        <InlineTitle>
            <h1>Go   </h1>
            <Select
              classNamePrefix="mySelect"
              options={dances}
              isClearable={true}
              backspaceRemovesValue={true}
            />
            <h1>   Dancing</h1>
        </InlineTitle>
        <InlineTitle>
            <h1>In   </h1>
            <Select 
              classNamePrefix="mySelect"
              options={locations}
              isClearable={true}
              backspaceRemovesValue={true}
            />
        </InlineTitle>
    </>
  )
}

export default IndexPage

export const Head: HeadFC = () => <title>Home Page</title>
