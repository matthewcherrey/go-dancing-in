import * as React from "react"

type Props = {
    children: JSX.Element | JSX.Element[];
};

const InlineTitle: React.FC<Props> = ({ children }) => (
  <div
    style={{
      margin: `0 auto`,
      padding: `var(--space-4) var(--size-gutter)`,
      display: `flex`,
      alignItems: `center`,
      textDecoration: `none`,
    }}
  >
    {children}
  </div>
)

export default InlineTitle